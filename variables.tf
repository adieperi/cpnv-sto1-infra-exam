variable "vpc_id" {
  description = "AWS VPC id"
  type        = string
  default     = "vpc-00bb8103516ccfc4b"
  nullable    = false
}

variable "group_number" {
  description = "Number of groups there will be during the exam"
  type        = string
  default     = "1"
  nullable    = false
}

variable "aws_provider_profile" {
  description = "AWS CLI profile name"
  type        = string
  default     = "default"
  nullable    = false
}

variable "dmz_ssh_allowed_ips" {
  description = "IP addresses allowed to connect to the infrastructure with SSH"
  type        = list(string)
  default     = ["0.0.0.0/0", "193.5.240.9/32"]
  nullable    = false
}

variable "aws_s3_buckets_name" {
  description = "A list with the desired AWS S3 buckets"
  type        = list(string)
  default     = ["sto1.sandbox.diduno.education"]
  nullable    = false
}

variable "aws_internet_gateway_name" {
  description = "AWS internet gateway name"
  type        = string
  default     = "sandbox_igw"
  nullable    = false
}

variable "tls_private_key_bits" {
  description = "Key size used to generate SSH keys"
  type        = string
  default     = "2048"
  nullable    = false
}

variable "tls_private_keys_name" {
  description = "Name of SSH private keys output files"
  type        = map(string)
  default = {
    admin = "id_rsa_admin"
    user  = "id_rsa_user_team"
  }
  nullable = false
}

variable "aws_key_pairs_name" {
  description = "Name of SSH public keys in AWS"
  type        = map(string)
  default = {
    admin = "sandbox_admin"
    user  = "sandbox_user_team"
  }
  nullable = false
}

variable "aws_route_table_public" {
  description = "AWS VPC public route table data"
  type        = map(string)
  default = {
    cidr_block = "0.0.0.0/0"
    name       = "sandbox_public"
  }
  nullable = false
}

variable "aws_route_table_private" {
  description = "AWS VPC private route table data"
  type        = map(string)
  default = {
    cidr_block = "0.0.0.0/0"
    name       = "sandbox_private"
  }
  nullable = false
}

variable "nat_instance_data" {
  description = "AWS EC2 NAT instance data"
  type        = map(string)
  default = {
    ami  = "ami-06d40f47626f889c8"
    type = "t3.nano"
    name = "sandbox_dmz_nat"
    ip   = "10.0.0.11"
  }
  nullable = false
}

variable "ssh_svr_instance_data" {
  description = "AWS EC2 linux SSH svr instance data"
  type        = map(string)
  default = {
    ami  = "ami-0acbd47ca29fbbc58"
    type = "t3.nano"
    name = "sandbox_dmz_ssh_svr"
    ip   = "10.0.0.12"
  }
  nullable = false
}

variable "linux_instance" {
  description = "AWS EC2 AMI type for linux instances"
  type        = map(string)
  default = {
    ami  = "ami-0acbd47ca29fbbc58"
    type = "t3.nano"
    name = "sandbox_linux"
  }
  nullable = false
}

variable "windows_instance" {
  description = "AWS EC2 AMI type for windows instances"
  type        = map(string)
  default = {
    ami  = "ami-016877168ef66b5a2"
    type = "t3.large"
    name = "sandbox_windows"
  }
  nullable = false
}

variable "aws_public_subnet_data" {
  description = "AWS Public VPC subnet CIDR and name"
  type        = map(string)
  default = {
    cidr_block = "10.0.0.0/24"
    name       = "sandbox_public"
  }
  nullable = false
}

variable "aws_private_subnet_name" {
  description = "AWS Private VPC subnet name"
  type        = string
  default     = "sandbox_private"
  nullable    = false
}

variable "ec2_linux_ebs_volume_size" {
  description = "AWS EC2 EBS linux volume size (in GiBs)"
  type        = string
  default     = "1"
  nullable    = false
}

variable "ec2_linux_ebs_volume_name" {
  description = "A list with AWS EC2 Linux instance EBS volume name (!! Must correspond to the number of disks indicated in the variable ec2_ebs_volume_count !!)"
  type        = list(string)
  default     = ["/dev/sdf", "/dev/sdg", "/dev/sdh", "/dev/sdi", "/dev/sdj"]
  nullable    = false
}

variable "ec2_windows_ebs_volume_size" {
  description = "AWS EC2 EBS Windows volume size (in GiBs)"
  type        = string
  default     = "5"
  nullable    = false
}

variable "ec2_windows_ebs_volume_name" {
  description = "A list with AWS EC2 Windows instance EBS volume name (!! Must correspond to the number of disks indicated in the variable ec2_ebs_volume_count !!)"
  type        = list(string)
  default     = ["xvdf", "xvdg", "xvdh", "xvdi", "xvdj"]
  nullable    = false
}
