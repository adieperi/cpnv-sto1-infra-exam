data "aws_vpc" "selected" {
  id = var.vpc_id
}

# Decrypt AWS EC2 Windows instances admin passwords
data "template_file" "decrypted_windows_passwords" {
  count = var.group_number

  template = rsadecrypt(tostring(element(aws_instance.windows[*].password_data, count.index)), file("${local.ssh_keys_path}/${var.tls_private_keys_name["admin"]}"))
}
