terraform {
  required_version = "~> 1.3.3"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "4.36.1"
    }
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.3"
    }
    local = {
      source  = "hashicorp/local"
      version = "2.2.3"
    }
    template = {
      source  = "hashicorp/template"
      version = "2.2.0"
    }
  }
}

provider "aws" {
  profile = var.aws_provider_profile
}
