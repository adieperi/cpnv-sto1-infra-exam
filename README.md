# cpnv-sto1-infra-exam

This project is my exam for the STO1 module at [CPNV ES](https://www.cpnv.ch/formations/ecole-superieure/informatique-systeme/)


## locals and data sources
All locals and data-sources are in separate files locals.tf and data-sources.tf respectively for readability reasons.

## Run Terraform project
### Requirements
**Download and configure aws-cli with valid credentials for the VPC vpc-00bb8103516ccfc4b**

### Download and init

```console
foo@bar:~$ git clone https://gitlab.com/adieperi/cpnv-sto1-infra-exam.git
foo@bar:~$ cd cpnv-sto1-infra-exam
foo@bar:~/cpnv-sto1-infra-exam$ mkdir -p ssh_key_files
foo@bar:~/cpnv-sto1-infra-exam$ touch ssh_key_files/id_rsa_admin
foo@bar:~/cpnv-sto1-infra-exam$ chmod 700 ssh_key_files/id_rsa_admin
foo@bar:~/cpnv-sto1-infra-exam$ terraform init
```

### Connect to private EC2 instances via DMZ
```console
foo@bar:~/cpnv-sto1-infra-exam$ ssh admin@<ssh_svr_ip> -i ./ssh_key_files/id_rsa_admin -L 127.0.0.1:2222:10.0.20.11:22 -L 127.0.0.1:3399:10.0.20.12:3389
foo@bar:~/cpnv-sto1-infra-exam$ ssh admin@127.0.0.1 -p 2222 -i ./ssh_key_files/id_rsa_admin
```

## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | ~> 1.3.3 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | 4.36.1 |
| <a name="requirement_local"></a> [local](#requirement\_local) | 2.2.3 |
| <a name="requirement_template"></a> [template](#requirement\_template) | 2.2.0 |
| <a name="requirement_tls"></a> [tls](#requirement\_tls) | 4.0.3 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 4.36.1 |
| <a name="provider_local"></a> [local](#provider\_local) | 2.2.3 |
| <a name="provider_template"></a> [template](#provider\_template) | 2.2.0 |
| <a name="provider_tls"></a> [tls](#provider\_tls) | 4.0.3 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_ebs_volume.linux](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/ebs_volume) | resource |
| [aws_ebs_volume.windows](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/ebs_volume) | resource |
| [aws_eip.nat_instance](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/eip) | resource |
| [aws_eip.ssh_svr](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/eip) | resource |
| [aws_instance.linux](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/instance) | resource |
| [aws_instance.nat](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/instance) | resource |
| [aws_instance.ssh_svr](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/instance) | resource |
| [aws_instance.windows](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/instance) | resource |
| [aws_internet_gateway.igw](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/internet_gateway) | resource |
| [aws_key_pair.admin](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/key_pair) | resource |
| [aws_key_pair.user](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/key_pair) | resource |
| [aws_network_interface.linux](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/network_interface) | resource |
| [aws_network_interface.nat_instance](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/network_interface) | resource |
| [aws_network_interface.ssh_svr](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/network_interface) | resource |
| [aws_network_interface.windows](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/network_interface) | resource |
| [aws_route_table.private](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/route_table) | resource |
| [aws_route_table.public](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/route_table) | resource |
| [aws_route_table_association.private](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/route_table_association) | resource |
| [aws_route_table_association.public](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/route_table_association) | resource |
| [aws_s3_bucket.b](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/s3_bucket) | resource |
| [aws_security_group.allow_all](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/security_group) | resource |
| [aws_security_group.allow_ssh](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/security_group) | resource |
| [aws_security_group.linux](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/security_group) | resource |
| [aws_security_group.windows](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/security_group) | resource |
| [aws_subnet.private](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/subnet) | resource |
| [aws_subnet.public](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/subnet) | resource |
| [aws_volume_attachment.linux](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/volume_attachment) | resource |
| [aws_volume_attachment.windows](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/resources/volume_attachment) | resource |
| [local_sensitive_file.id_rsa_admin](https://registry.terraform.io/providers/hashicorp/local/2.2.3/docs/resources/sensitive_file) | resource |
| [local_sensitive_file.id_rsa_user](https://registry.terraform.io/providers/hashicorp/local/2.2.3/docs/resources/sensitive_file) | resource |
| [tls_private_key.keygen_admin](https://registry.terraform.io/providers/hashicorp/tls/4.0.3/docs/resources/private_key) | resource |
| [tls_private_key.keygen_user](https://registry.terraform.io/providers/hashicorp/tls/4.0.3/docs/resources/private_key) | resource |
| [aws_vpc.selected](https://registry.terraform.io/providers/hashicorp/aws/4.36.1/docs/data-sources/vpc) | data source |
| [template_file.decrypted_windows_passwords](https://registry.terraform.io/providers/hashicorp/template/2.2.0/docs/data-sources/file) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aws_internet_gateway_name"></a> [aws\_internet\_gateway\_name](#input\_aws\_internet\_gateway\_name) | AWS internet gateway name | `string` | `"sandbox_igw"` | no |
| <a name="input_aws_key_pairs_name"></a> [aws\_key\_pairs\_name](#input\_aws\_key\_pairs\_name) | Name of SSH public keys in AWS | `map(string)` | <pre>{<br>  "admin": "sandbox_admin",<br>  "user": "sandbox_user_team"<br>}</pre> | no |
| <a name="input_aws_private_subnet_name"></a> [aws\_private\_subnet\_name](#input\_aws\_private\_subnet\_name) | AWS Private VPC subnet name | `string` | `"sandbox_private"` | no |
| <a name="input_aws_provider_profile"></a> [aws\_provider\_profile](#input\_aws\_provider\_profile) | AWS CLI profile name | `string` | `"default"` | no |
| <a name="input_aws_public_subnet_data"></a> [aws\_public\_subnet\_data](#input\_aws\_public\_subnet\_data) | AWS Public VPC subnet CIDR and name | `map(string)` | <pre>{<br>  "cidr_block": "10.0.0.0/24",<br>  "name": "sandbox_public"<br>}</pre> | no |
| <a name="input_aws_route_table_private"></a> [aws\_route\_table\_private](#input\_aws\_route\_table\_private) | AWS VPC private route table data | `map(string)` | <pre>{<br>  "cidr_block": "0.0.0.0/0",<br>  "name": "sandbox_private"<br>}</pre> | no |
| <a name="input_aws_route_table_public"></a> [aws\_route\_table\_public](#input\_aws\_route\_table\_public) | AWS VPC public route table data | `map(string)` | <pre>{<br>  "cidr_block": "0.0.0.0/0",<br>  "name": "sandbox_public"<br>}</pre> | no |
| <a name="input_aws_s3_buckets_name"></a> [aws\_s3\_buckets\_name](#input\_aws\_s3\_buckets\_name) | A list with the desired AWS S3 buckets | `list(string)` | <pre>[<br>  "sto1.sandbox.diduno.education"<br>]</pre> | no |
| <a name="input_dmz_ssh_allowed_ips"></a> [dmz\_ssh\_allowed\_ips](#input\_dmz\_ssh\_allowed\_ips) | IP addresses allowed to connect to the infrastructure with SSH | `list(string)` | <pre>[<br>  "0.0.0.0/0",<br>  "193.5.240.9/32"<br>]</pre> | no |
| <a name="input_ec2_linux_ebs_volume_name"></a> [ec2\_linux\_ebs\_volume\_name](#input\_ec2\_linux\_ebs\_volume\_name) | A list with AWS EC2 Linux instance EBS volume name (!! Must correspond to the number of disks indicated in the variable ec2\_ebs\_volume\_count !!) | `list(string)` | <pre>[<br>  "/dev/sdf",<br>  "/dev/sdg",<br>  "/dev/sdh",<br>  "/dev/sdi",<br>  "/dev/sdj"<br>]</pre> | no |
| <a name="input_ec2_linux_ebs_volume_size"></a> [ec2\_linux\_ebs\_volume\_size](#input\_ec2\_linux\_ebs\_volume\_size) | AWS EC2 EBS linux volume size (in GiBs) | `string` | `"1"` | no |
| <a name="input_ec2_windows_ebs_volume_name"></a> [ec2\_windows\_ebs\_volume\_name](#input\_ec2\_windows\_ebs\_volume\_name) | A list with AWS EC2 Windows instance EBS volume name (!! Must correspond to the number of disks indicated in the variable ec2\_ebs\_volume\_count !!) | `list(string)` | <pre>[<br>  "xvdf",<br>  "xvdg",<br>  "xvdh",<br>  "xvdi",<br>  "xvdj"<br>]</pre> | no |
| <a name="input_ec2_windows_ebs_volume_size"></a> [ec2\_windows\_ebs\_volume\_size](#input\_ec2\_windows\_ebs\_volume\_size) | AWS EC2 EBS Windows volume size (in GiBs) | `string` | `"5"` | no |
| <a name="input_group_number"></a> [group\_number](#input\_group\_number) | Number of groups there will be during the exam | `string` | `"1"` | no |
| <a name="input_linux_instance"></a> [linux\_instance](#input\_linux\_instance) | AWS EC2 AMI type for linux instances | `map(string)` | <pre>{<br>  "ami": "ami-0acbd47ca29fbbc58",<br>  "name": "sandbox_linux",<br>  "type": "t3.nano"<br>}</pre> | no |
| <a name="input_nat_instance_data"></a> [nat\_instance\_data](#input\_nat\_instance\_data) | AWS EC2 NAT instance data | `map(string)` | <pre>{<br>  "ami": "ami-06d40f47626f889c8",<br>  "ip": "10.0.0.11",<br>  "name": "sandbox_dmz_nat",<br>  "type": "t3.nano"<br>}</pre> | no |
| <a name="input_ssh_svr_instance_data"></a> [ssh\_svr\_instance\_data](#input\_ssh\_svr\_instance\_data) | AWS EC2 linux SSH svr instance data | `map(string)` | <pre>{<br>  "ami": "ami-0acbd47ca29fbbc58",<br>  "ip": "10.0.0.12",<br>  "name": "sandbox_dmz_ssh_svr",<br>  "type": "t3.nano"<br>}</pre> | no |
| <a name="input_tls_private_key_bits"></a> [tls\_private\_key\_bits](#input\_tls\_private\_key\_bits) | Key size used to generate SSH keys | `string` | `"2048"` | no |
| <a name="input_tls_private_keys_name"></a> [tls\_private\_keys\_name](#input\_tls\_private\_keys\_name) | Name of SSH private keys output files | `map(string)` | <pre>{<br>  "admin": "id_rsa_admin",<br>  "user": "id_rsa_user_team"<br>}</pre> | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | AWS VPC id | `string` | `"vpc-00bb8103516ccfc4b"` | no |
| <a name="input_windows_instance"></a> [windows\_instance](#input\_windows\_instance) | AWS EC2 AMI type for windows instances | `map(string)` | <pre>{<br>  "ami": "ami-016877168ef66b5a2",<br>  "name": "sandbox_windows",<br>  "type": "t3.large"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_ssh_svr_instance_ip"></a> [ssh\_svr\_instance\_ip](#output\_ssh\_svr\_instance\_ip) | Public IP of AWS SSH svr EC2 instance |
| <a name="output_windows_instances_passwords"></a> [windows\_instances\_passwords](#output\_windows\_instances\_passwords) | Windows EC2 instances decrypted passwords |

## License

This project is licensed under MIT License. See [LICENSE](/LICENSE) for more details.

## Maintainers and Contributors

- [Anthony Dieperink](https://gitlab.com/adieperi)
