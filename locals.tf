locals {
  ssh_keys_path         = "${path.module}/ssh_key_files"
  ssh_svr_instance_cidr = "${var.ssh_svr_instance_data["ip"]}/32"
}
