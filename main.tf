# Creation of an AWS public subnet
resource "aws_subnet" "public" {
  vpc_id     = data.aws_vpc.selected.id
  cidr_block = var.aws_public_subnet_data["cidr_block"]

  tags = {
    Name = var.aws_public_subnet_data["name"]
  }
}

# Creation of an AWS private subnet
resource "aws_subnet" "private" {
  count = var.group_number

  vpc_id     = data.aws_vpc.selected.id
  cidr_block = "10.0.2${count.index}.0/24"

  tags = {
    Name = "${var.aws_private_subnet_name}_${count.index}"
  }
}

# Creation of an AWS internet gateway
resource "aws_internet_gateway" "igw" {
  vpc_id = data.aws_vpc.selected.id

  tags = {
    Name = var.aws_internet_gateway_name
  }
}

# Creation of an AWS route table for public subnet
resource "aws_route_table" "public" {
  vpc_id = data.aws_vpc.selected.id

  route {
    cidr_block = var.aws_route_table_public["cidr_block"]
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = {
    Name = var.aws_route_table_public["name"]
  }
}

# Creation of an AWS route table for private subnet
resource "aws_route_table" "private" {
  vpc_id = data.aws_vpc.selected.id

  route {
    cidr_block           = var.aws_route_table_private["cidr_block"]
    network_interface_id = aws_network_interface.nat_instance.id
  }

  tags = {
    Name = var.aws_route_table_private["name"]
  }
}

# Associate AWS public subnet with the AWS public route table
resource "aws_route_table_association" "public" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.public.id
}

# Associate AWS private subnet with the AWS private route table
resource "aws_route_table_association" "private" {
  count = var.group_number

  subnet_id      = aws_subnet.private[count.index].id
  route_table_id = aws_route_table.private.id
}

resource "aws_security_group" "allow_ssh" {
  name        = "dmz_ssh"
  description = "Allow inbound SSH traffic to the DMZ"
  vpc_id      = data.aws_vpc.selected.id

  dynamic "ingress" {
    for_each = var.dmz_ssh_allowed_ips

    content {
      from_port   = 22
      to_port     = 22
      cidr_blocks = [ingress.value]
      protocol    = "tcp"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "allow_all" {
  name        = "dmz_nat"
  description = "Allow all private subnets to go out on the internet with all protocols"
  vpc_id      = data.aws_vpc.selected.id

  dynamic "ingress" {
    for_each = aws_subnet.private[*].cidr_block

    content {
      from_port   = 0
      to_port     = 0
      cidr_blocks = [ingress.value]
      protocol    = "-1"
    }
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "linux" {
  name        = "private_subnet_linux"
  description = "Allow the EC2 SSH svr instance (${local.ssh_svr_instance_cidr}) to connect with SSH protocol"
  vpc_id      = data.aws_vpc.selected.id

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [local.ssh_svr_instance_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "windows" {
  name        = "private_subnet_windows"
  description = "Allow the EC2 SSH svr instance (${local.ssh_svr_instance_cidr}) to connect with RDP protocol"
  vpc_id      = data.aws_vpc.selected.id

  ingress {
    from_port   = 3389
    to_port     = 3389
    protocol    = "tcp"
    cidr_blocks = [local.ssh_svr_instance_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

# Creation of a SSH key pair for administrator
resource "tls_private_key" "keygen_admin" {
  algorithm = "RSA"
  rsa_bits  = var.tls_private_key_bits
}

# Creation of a SSH key pairs for users
resource "tls_private_key" "keygen_user" {
  count = var.group_number

  algorithm = "RSA"
  rsa_bits  = var.tls_private_key_bits
}

# Save the private key created for the administrator to a file
resource "local_sensitive_file" "id_rsa_admin" {
  content  = tls_private_key.keygen_admin.private_key_openssh
  filename = "${local.ssh_keys_path}/${var.tls_private_keys_name["admin"]}"
}

# Save the private keys created for the users to a file
resource "local_sensitive_file" "id_rsa_user" {
  count = var.group_number

  content  = tls_private_key.keygen_user[count.index].private_key_openssh
  filename = "${local.ssh_keys_path}/${var.tls_private_keys_name["user"]}_${count.index}"
}

# Register the public key created for the administrator on AWS
resource "aws_key_pair" "admin" {
  key_name   = var.aws_key_pairs_name["admin"]
  public_key = tls_private_key.keygen_admin.public_key_openssh
}

# Register the public keys created for the users on AWS
resource "aws_key_pair" "user" {
  count = var.group_number

  key_name   = "${var.aws_key_pairs_name["user"]}_${count.index}"
  public_key = tls_private_key.keygen_user[count.index].public_key_openssh
}

# Creation of S3 buckets
resource "aws_s3_bucket" "b" {
  for_each = toset(var.aws_s3_buckets_name)

  bucket = each.value
}

# Creation of the network interface for the AWS EC2 NAT instance
resource "aws_network_interface" "nat_instance" {
  subnet_id         = aws_subnet.public.id
  private_ips       = [var.nat_instance_data["ip"]]
  security_groups   = [aws_security_group.allow_all.id]
  source_dest_check = false
}

# Creation of the AWS EC2 NAT instance
resource "aws_instance" "nat" {
  ami           = var.nat_instance_data["ami"]
  instance_type = var.nat_instance_data["type"]
  key_name      = aws_key_pair.admin.id

  network_interface {
    network_interface_id = aws_network_interface.nat_instance.id
    device_index         = 0
  }

  tags = {
    Name = var.nat_instance_data["name"]
  }
}

# Creation of the AWS Elastic IP for the AWS EC2 NAT instance
resource "aws_eip" "nat_instance" {
  vpc                       = true
  instance                  = aws_instance.nat.id
  associate_with_private_ip = var.nat_instance_data["ip"]
  depends_on                = [aws_internet_gateway.igw]
}

# Creation of the network interface for the AWS EC2 SSH svr instance
resource "aws_network_interface" "ssh_svr" {
  subnet_id       = aws_subnet.public.id
  private_ips     = [var.ssh_svr_instance_data["ip"]]
  security_groups = [aws_security_group.allow_ssh.id]

}

# Creation of the AWS EC2 SSH svr instance
resource "aws_instance" "ssh_svr" {
  ami           = var.ssh_svr_instance_data["ami"]
  instance_type = var.ssh_svr_instance_data["type"]
  key_name      = aws_key_pair.admin.id

  network_interface {
    network_interface_id = aws_network_interface.ssh_svr.id
    device_index         = 0
  }

  tags = {
    Name = var.ssh_svr_instance_data["name"]
  }
}

# Creation of the AWS Elastic IP for the AWS EC2 SSH svr instance
resource "aws_eip" "ssh_svr" {
  vpc                       = true
  instance                  = aws_instance.ssh_svr.id
  associate_with_private_ip = var.ssh_svr_instance_data["ip"]
  depends_on                = [aws_internet_gateway.igw]
}

# Creation of the network interfaces for the AWS EC2 Linux instances
resource "aws_network_interface" "linux" {
  count = var.group_number

  subnet_id       = aws_subnet.private[count.index].id
  private_ips     = ["10.0.2${count.index}.11"]
  security_groups = [aws_security_group.linux.id]
}

# Creation of the AWS EC2 Linux instances
resource "aws_instance" "linux" {
  count = var.group_number

  ami           = var.linux_instance["ami"]
  instance_type = var.linux_instance["type"]
  key_name      = aws_key_pair.admin.id

  network_interface {
    network_interface_id = aws_network_interface.linux[count.index].id
    device_index         = 0
  }

  timeouts {
    create = "120m"
    update = "120m"
    delete = "120m"
  }

  tags = {
    Name = "${var.linux_instance["name"]}_${count.index}"
  }
}

# Creation of AWS EBS volumes for AWS EC2 Linux instances
resource "aws_ebs_volume" "linux" {
  count = var.group_number * length(var.ec2_linux_ebs_volume_name)

  availability_zone = element(aws_instance.linux[*].availability_zone, count.index)
  size              = var.ec2_linux_ebs_volume_size

  tags = {
    Name = element(var.ec2_linux_ebs_volume_name, count.index)
  }
}

# Associate AWS EBS volumes with the appropriate AWS EC2 Linux instance
resource "aws_volume_attachment" "linux" {
  count = var.group_number * length(var.ec2_linux_ebs_volume_name)

  volume_id   = [for o in aws_ebs_volume.linux : o.id][count.index]
  device_name = element(var.ec2_linux_ebs_volume_name, count.index)
  instance_id = element(aws_instance.linux[*].id, count.index)
}

# Creation of the network interfaces for the AWS EC2 Windows instances
resource "aws_network_interface" "windows" {
  count = var.group_number

  subnet_id       = aws_subnet.private[count.index].id
  private_ips     = ["10.0.2${count.index}.12"]
  security_groups = [aws_security_group.windows.id]
}

# Creation of the AWS EC2 Windows instances
resource "aws_instance" "windows" {
  count = var.group_number

  ami               = var.windows_instance["ami"]
  instance_type     = var.windows_instance["type"]
  key_name          = aws_key_pair.admin.id
  get_password_data = true

  network_interface {
    network_interface_id = aws_network_interface.windows[count.index].id
    device_index         = 0
  }

  timeouts {
    create = "120m"
    update = "120m"
    delete = "120m"
  }

  tags = {
    Name = "${var.windows_instance["name"]}_${count.index}"
  }
}

# Creation of AWS EBS volumes for AWS EC2 Windows instances
resource "aws_ebs_volume" "windows" {
  count = var.group_number * length(var.ec2_windows_ebs_volume_name)

  availability_zone = element(aws_instance.windows[*].availability_zone, count.index)
  size              = var.ec2_windows_ebs_volume_size

  tags = {
    Name = element(var.ec2_windows_ebs_volume_name, count.index)
  }
}

# Associate AWS EBS volumes with the appropriate AWS EC2 Windows instance
resource "aws_volume_attachment" "windows" {
  count = var.group_number * length(var.ec2_windows_ebs_volume_name)

  volume_id   = [for o in aws_ebs_volume.windows : o.id][count.index]
  device_name = element(var.ec2_windows_ebs_volume_name, count.index)
  instance_id = element(aws_instance.windows[*].id, count.index)
}
