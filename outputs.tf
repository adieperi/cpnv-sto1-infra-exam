output "ssh_svr_instance_ip" {
  description = "Public IP of AWS SSH svr EC2 instance"
  value       = aws_eip.ssh_svr.public_ip
}

# The first password displayed corresponds to the first AWS Windows EC2 instance and so on
output "windows_instances_passwords" {
  description = "Windows EC2 instances decrypted passwords"
  value       = [for o in data.template_file.decrypted_windows_passwords : o.rendered][*]
}
